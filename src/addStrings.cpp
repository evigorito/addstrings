#include <Rcpp.h>
#include <string>
using namespace Rcpp;

//' Get unique pairs of haplotypes from a set of haplotypes
//'
//' This function allows you to sum a pair of haplotypes (strings) and return a string with the sum. For speed, the function assumes that the two strings are the same length and contains only the chars '0' and '1'., written by Colin Starr
//' @param a vector with haplotypes 1
//' @param b vector with haplotypes 
//' @keywords sum haplotype pairs
//' @name addStrings
//' @export
//' @return vector  with the sum of the haplotype pair (genotype)

// [[Rcpp::export]]
std::vector<std::string> addStrings(std::vector<std::string> &one, 
std::vector<std::string> &two) {
     std::vector<std::string> out(one.size());
     for (unsigned i = 0; i < one.size(); i++) {
         out[i].resize(one[i].size());
         for (unsigned j = 0; j < one[i].size(); j++) {
             out[i][j] = one[i][j] + two[i][j] - '0';
         }
     }
     return out;
}


