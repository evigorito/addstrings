#' The 'addstrings' package
#'
#' @description adding haplotypes
#' @docType package
#' @name addstrings-package
#' @aliases addstrings
#' @useDynLib addstrings, .registration = TRUE
#' @import data.table
#' @import methods
#' @import Rcpp
NULL
